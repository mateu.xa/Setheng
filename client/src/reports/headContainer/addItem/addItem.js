import './addItem.css';
import React, { useState } from 'react';
import axios from 'axios';



function AddItem(props) {
    const [inputName, setinputName] = useState();
    const [inputEmail, setinputEmail] = useState();
    const [inputPassword, setinputPassword] = useState();
    const [inputConfirmPassword, setinputConfirmPassword] = useState();
    const [inputTaxId, setinputTaxId] = useState();

    function handleNameChange(e) {
        setinputName({inputName: e.target.value});
    }

    function handleEmailChange(e) {
        setinputEmail({inputEmail: e.target.value});
    }

    function handlePasswordChange(e) {
        setinputPassword({inputPassword: e.target.value});
    }

    function handleConfirmPasswordChange(e) {
        setinputConfirmPassword({inputConfirmPassword: e.target.value});
    }

    function handleTaxIdChange(e) {
        setinputTaxId({inputTaxId: e.target.value});
    }

    const payload = () => {

        let jsonItems = {}

        if(typeof(inputName) !== 'undefined'){
            jsonItems['companyName'] = String(inputName.inputName)
        }
        if(typeof(inputEmail) !== 'undefined'){
            jsonItems['email'] = String(inputEmail.inputEmail)
        }
        if(typeof(inputPassword) !== 'undefined'){
            jsonItems['password'] = String(inputPassword.inputPassword)
        }
        if(typeof(inputTaxId) !== 'undefined'){
            jsonItems['cnpj'] = String(inputTaxId.inputTaxId)
        }

        return jsonItems
    }

    return (props.trigger) ? (
        <div className="item-popup">

            <div id={props.id} class="form">

                <div className='item-popup-header'>
                    <p className='item-popup-header-id'>ADICIONAR PRODUTO</p>

                    <img src={process.env.PUBLIC_URL + '/xmark-solid.svg'} className='item-popup-header-close' onClick={() => props.setTrigger(false)} />
                </div>

                <div class="input-container ic1">
                    <input id={"name-" + props.id} class="input" type="text" required placeholder=" " onChange={(e)=> handleNameChange(e)} />
                
                    <label for="name" class="placeholder">Nome</label>
                </div>

                <div class="input-container ic2">
                    <input id={"email-" + props.id} class="input" type="email" required placeholder=" " onChange={(e)=> handleEmailChange(e)} />
                
                    <label for="email" class="placeholder">E-mail</label>
                </div>

                <div class="input-container ic2">
                    <input id={"password-" + props.id} class="input" type="password" required placeholder=" " onChange={(e)=> handlePasswordChange(e)} />
                
                    <label for="password" class="placeholder">Senha</label>
                </div>
                
                <div class="input-container ic2">
                    <input id={"confirmPassword-" + props.id} class="input" type="password" required placeholder=" " onChange={(e)=> handleConfirmPasswordChange(e)} />
                
                    <label for="confirmPassword" class="placeholder">Confirme a senha</label>
                </div>

                <div class="input-container ic2">
                    <input id={"taxId-" + props.id} class="input" type="number" min="8" max="8" required placeholder=" " onChange={(e)=> handleTaxIdChange(e)} />
                
                    <label for="taxId" class="placeholder">CNPJ</label>
                </div>

                <button type="text" class="button" onClick={async ()=>{

                    if(inputPassword.inputPassword == inputConfirmPassword.inputConfirmPassword){

                        await axios.post(`${process.env.REACT_APP_ENDPOINT}/api/v1/company`,
                        payload())
                        .then((response) => {
                            if(response.status === 200){
                                alert("Empresa adicionado com sucesso!")
                                window.location.reload(false)
                            }else{
                                alert("Ocorreu um erro, tente mais tarde novamente.")
                            }
                            // console.log(response.status)
                        })
                    }else{
                        alert("Campos de senha diferem!")
                    }

                }}>Adicionar</button>
            </div>
        </div>
    ) : "";
}

export default AddItem;