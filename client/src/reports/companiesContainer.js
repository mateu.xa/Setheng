import './companiesContainer.css';
import React from 'react';
import HeadContainer from './headContainer/headContainer';
import BodyContainer from './bodyContainer/bodyContainer';

function Companies() {
  return (
    <div className="main-container">
        <HeadContainer />
        <BodyContainer />
    </div>
  );
}

export default Companies;