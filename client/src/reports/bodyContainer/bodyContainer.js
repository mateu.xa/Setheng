import './bodyContainer.css';
import ListItem from './listItem/listItem';
import React, { useState } from 'react';
const axios = require('axios')

function BodyContainer() {

    const [items, setItems] = useState([])

    const [loading, setLoading] = useState(false)

    const [searchValue, setSeracValue] = useState('')

    let search = async val => {
        setLoading(true)

        const res = await axios.get(`${process.env.REACT_APP_ENDPOINT}/api/v1/companies?limit=10&query=${val}`)

        setItems(await res.data)

        setLoading(false)
    }


    let onChangeHandler = async e => {
        search(e.target.value)
        setSeracValue(e.target.value)
    }

    let renderItems = () => {
        if(loading){
            return `<p>Loading...</p>`
        }else{
            if(typeof(items) === "undefined"){
                return "Nenhum item encontrado"
            }else{
                return items.map(item => (
                    <>
                        <ListItem name={item.companyName} id={item.id} createdAt={item.createdAt} />
                    </>
                ))
            }
        }
    }

    React.useEffect(() => {
        axios.get(`${process.env.REACT_APP_ENDPOINT}/api/v1/companies?limit=10`)
        .then((response) => {
            setItems(response.data)
        })
    }, [])

    return (
        <div className="body-container">
            <input 
                value={searchValue}
                onChange={e => onChangeHandler(e)}
                placeholder="Digite para procurar"
            />
            {renderItems()}
        </div>
    )
}

export default BodyContainer;