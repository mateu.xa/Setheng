import './listItem.css';
import ItemPopup from '../itemPopup/itemPopup'
import React, { useState } from 'react';

function ListItem(props) {

  const [buttonPopup, setButtonPopup] = useState(false)

  return (
    <>
      <div onClick={() => setButtonPopup(true)} className="list-item">
        <p>{props.name}</p>
        <p>id: {props.id}</p>
        <p>{props.createdAt}</p>
      </div>
      <ItemPopup trigger={buttonPopup} setTrigger={setButtonPopup} id={props.id} />
    </>
  );
}

export default ListItem;