import './itemPopup.css';
import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';



function ItemPopup(props) {
    const [items, setItems] = useState([]);
    const [inputName, setinputName] = useState();
    const [inputEmail, setinputEmail] = useState();
    const [inputPassword, setinputPassword] = useState();
    const [inputTaxId, setinputTaxId] = useState();

    React.useEffect(() => {
        axios.get(`${process.env.REACT_APP_ENDPOINT}/api/v1/company/${props.id}}`)
        .then((response) => {
            setItems(response.data)
        })
    }, [])

    function handleNameChange(e) {
        setinputName({inputName: e.target.value});
    }

    function handleEmailChange(e) {
        setinputEmail({inputEmail: e.target.value});
    }

    function handlePasswordChange(e) {
        setinputPassword({inputPassword: e.target.value});
    }

    function handleTaxIdChange(e) {
        setinputTaxId({inputTaxId: e.target.value});
    }

    const payload = () => {

        let jsonItems = {}

        if(typeof(inputName) !== 'undefined'){
            jsonItems['companyName'] = String(inputName.inputName)
        }
        if(typeof(inputEmail) !== 'undefined'){
            jsonItems['email'] = String(inputEmail.inputEmail)
        }
        if(typeof(inputPassword) !== 'undefined'){
            jsonItems['password'] = String(inputPassword.inputPassword)
        }
        if(typeof(inputTaxId) !== 'undefined'){
            jsonItems['cnpj'] = String(inputTaxId.inputTaxId)
        }

        return jsonItems
    }

    return (props.trigger) ? (
        <div className="item-popup">

            <div id={props.id} class="form">
                <div className='item-popup-header'>
                    <p className='item-popup-header-id'>ID: {items.id}</p>

                    <img src={process.env.PUBLIC_URL + '/xmark-solid.svg'} className='item-popup-header-close' onClick={() => props.setTrigger(false)} />
                </div>

                <Link to={'/reports/' + props.id} >
                    Acessar Relatórios
                </Link>

                <div class="title">{items.companyName}</div>

                <div class="subtitle">{items.email}</div>

                <div class="subtitle">{items.password}</div>

                <div class="subtitle">{items.cnpj}</div>

                <div class="input-container ic1">
                    <input id={"name-" + props.id} class="input" type="text" required placeholder=" " onChange={(e)=> handleNameChange(e)} />
                
                    <label for="name" class="placeholder">Nome</label>
                </div>

                <div class="input-container ic2">
                    <input id={"email-" + props.id} class="input" type="email" required placeholder=" " onChange={(e)=> handleEmailChange(e)} />
                
                    <label for="email" class="placeholder">E-mail</label>
                </div>

                <div class="input-container ic2">
                    <input id={"password-" + props.id} class="input" type="password" required placeholder=" " onChange={(e)=> handlePasswordChange(e)} />
                
                    <label for="password" class="placeholder">Senha</label>
                </div>

                <div class="input-container ic2">
                    <input id={"taxId-" + props.id} class="input" type="number" min="8" max="8" required placeholder=" " onChange={(e)=> handleTaxIdChange(e)} />
                
                    <label for="taxId" class="placeholder">CNPJ</label>
                </div>

                <button type="text" class="button" onClick={async ()=>{

                    await axios.patch(`${process.env.REACT_APP_ENDPOINT}/api/v1/company/${props.id}`,
                        payload()
                    ).then((response) => {
                        if(response.status === 200){
                            alert("Empresa alterado com sucesso!")
                            window.location.reload(false)
                        }else{
                            alert("Ocorreu um erro, tente mais tarde novamente.")
                        }
                        // console.log(response.status)
                    })
                }}>Salvar</button>
            </div>
        </div>
    ) : "";
}

export default ItemPopup;
