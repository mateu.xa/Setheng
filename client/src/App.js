import './App.css';
import React from 'react';
import Sidebar from './sidebar/sidebar'
import Products from './products/productContainer';
import Companies from './companies/companiesContainer';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

function App() {
  return (
    <Router>
      <div className="App">
        <Sidebar />
          <Routes>
            <Route path='/products' element={<Products />} />
            <Route path='/companies' element={<Companies />} />
          </Routes>
      </div>
    </Router>
  );
}

export default App;
