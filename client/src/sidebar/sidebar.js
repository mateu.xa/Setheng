import './sidebar.css';
import React from 'react';
import { Link } from 'react-router-dom'

function Sidebar() {
  return (
    <div className="sidebar">
      <div className='sidebar-container'>
        <Link className='sidebar-icon' to='/products'>
          <img src={process.env.PUBLIC_URL + '/basket-shopping-solid.svg'} />
        </Link>
        <Link className='sidebar-icon' to='/companies'>
          <img src={process.env.PUBLIC_URL + '/building-solid.svg'} />
        </Link>
      </div>
    </div>
  );
}

export default Sidebar;