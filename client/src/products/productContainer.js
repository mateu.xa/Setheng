import './productContainer.css';
import React from 'react';
import HeadContainer from './headContainer/headContainer';
import BodyContainer from './bodyContainer/bodyContainer';

function Products() {
  return (
    <div className="main-container">
        <HeadContainer />
        <BodyContainer />
    </div>
  );
}

export default Products;