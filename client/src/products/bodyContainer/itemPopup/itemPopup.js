import './itemPopup.css';
import React, { useState } from 'react';
import axios from 'axios';



function ItemPopup(props) {
    const [items, setItems] = useState([]);
    const [inputName, setinputName] = useState();
    const [inputDescription, setinputDescription] = useState();
    const [inputImage, setinputImage] = useState();

    React.useEffect(() => {
        axios.get(`${process.env.REACT_APP_ENDPOINT}/api/v1/product/${props.id}}`)
        .then((response) => {
            setItems(response.data)
        })
    }, [])

    function handleNameChange(e) {
        setinputName({inputName: e.target.value});
    }

    function handleDescriptionChange(e) {
        setinputDescription({inputDescription: e.target.value});
    }

    function handleImageChange(e){
        setinputImage({inputImage: e.target.files[0]});
    }

    const payload = () => {
        let jsonItems = {}

        if(typeof(inputName) !== 'undefined'){
            jsonItems['name'] = String(inputName.inputName)
        }
        if(typeof(inputDescription) !== 'undefined'){
            jsonItems['description'] = String(inputDescription.inputDescription)
        }
        if(typeof(inputImage) !== 'undefined'){
            jsonItems['image'] = String(inputImage.inputImage)
        }

        return jsonItems
    }

    return (props.trigger) ? (
        <div className="item-popup">

            <div id={props.id} class="form">
                <div className='item-popup-header'>
                    <p className='item-popup-header-id'>ID: {items.id}</p>

                    <img src={process.env.PUBLIC_URL + '/xmark-solid.svg'} className='item-popup-header-close' onClick={() => props.setTrigger(false)} />
                </div>

                <div class="title">{items.name}</div>

                <div class="subtitle">{items.description}</div>

                <input type="file" onChange={(e) => handleImageChange(e)}/>

                <div class="input-container ic1">
                    <input id={"name-" + props.id} class="input" type="text" placeholder=" " onChange={(e)=> handleNameChange(e)} />
                
                    <label for="name" class="placeholder">Name</label>
                </div>

                <div class="input-container ic2">
                    <textarea id={"description-" + props.id} class="input" placeholder=" " onChange={(e)=> handleDescriptionChange(e)} ></textarea>
                
                    <label for="description" class="placeholder">Description</label>
                </div>

                <button type="text" class="button" onClick={async ()=>{
                    await axios.patch(`${process.env.REACT_APP_ENDPOINT}/api/v1/product/${props.id}`,
                    payload()
                    ).then((response) => {
                        if(response.status === 200){
                            alert("Produto alterado com sucesso!")
                            window.location.reload(false)
                        }else{
                            alert("Ocorreu um erro, tente mais tarde novamente.")
                        }
                        // console.log(response.status)
                    })
                }}>Salvar</button>
            </div>
        </div>
    ) : "";
}

export default ItemPopup;
