import './headContainer.css';
import AddItem from './addItem/addItem'
import RemoveItem from './removeItem/removeItem';
import React, { useState } from 'react';


function HeadContainer() {
    const [buttonAddPopup, setButtonAddPopup] = useState(false)
    const [buttonRemovePopup, setButtonRemovePopup] = useState(false)

    return (
        <>
            <div className="head-container">
                <a onClick={() => {
                    if(buttonRemovePopup) setButtonRemovePopup(false)
                    setButtonAddPopup(true)
                }}>Adicionar produto</a>
                
                <a onClick={() => {
                    if(buttonAddPopup) setButtonAddPopup(false)
                    setButtonRemovePopup(true)
                }}>Remover produto</a>

                <input type="text" />

                <a href="">Sair</a>
            </div>

            <div className='AddRemoveItem' >
                <AddItem trigger={buttonAddPopup} setTrigger={setButtonAddPopup} id="AddItem" />
                <RemoveItem trigger={buttonRemovePopup} setTrigger={setButtonRemovePopup} id="RemoveItem" />
            </div>
        </>
    );
}

export default HeadContainer;