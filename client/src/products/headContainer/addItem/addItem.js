import './addItem.css';
import React, { useState } from 'react';
import axios from 'axios';



function AddItem(props) {
    const [inputName, setinputName] = useState();
    const [inputDescription, setinputDescription] = useState();
    const [inputFile, setinputFile] = useState();

    function handleNameChange(e) {
        setinputName({inputName: e.target.value});
    }

    function handleDescriptionChange(e) {
        setinputDescription({inputDescription: e.target.value});
    }

    function handleFileChange(e){
        const inputFile = {
            preview: URL.createObjectURL(e.target.files[0]),
            data: e.target.files[0],
        };
        setinputFile(inputFile);
    }


    return (props.trigger) ? (
        <div className="item-popup">

            <div id={props.id} class="form">

                <div className='item-popup-header'>
                    <p className='item-popup-header-id'>ADICIONAR PRODUTO</p>

                    <img src={process.env.PUBLIC_URL + '/xmark-solid.svg'} className='item-popup-header-close' onClick={() => props.setTrigger(false)} />
                </div>

                <input type="file" onChange={(e)=> handleFileChange(e)} />

                <div class="input-container ic1">
                    <input id={"name-" + props.id} class="input" type="text" placeholder=" " onChange={(e)=> handleNameChange(e)} />
                
                    <label for="name" class="placeholder">Name</label>
                </div>

                <div class="input-container ic2">
                    <textarea id={"description-" + props.id} class="input" placeholder=" " onChange={(e)=> handleDescriptionChange(e)} ></textarea>
                
                    <label for="description" class="placeholder">Description</label>
                </div>

                <button type="text" class="button" onClick={async ()=>{

                const form = new FormData();

                form.append("name", String(inputName.inputName))
                form.append("description", String(inputDescription.inputDescription))
                form.append("file", inputFile.data);

                    await axios.post(`${process.env.REACT_APP_ENDPOINT}/api/v1/product`,
                    form,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then((response) => {
                        console.log(response)
                        if(response.status === 200){
                            alert("Produto adicionado com sucesso!")
                            window.location.reload(false)
                        }else{
                            alert("Ocorreu um erro, tente mais tarde novamente.")
                        }
                        console.log(response.status)
                    })
                }}>Adicionar</button>
            </div>
        </div>
    ) : "";
}

export default AddItem;