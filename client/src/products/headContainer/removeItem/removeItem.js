import './removeItem.css';
import React, { useState } from 'react';
import axios from 'axios';


function RemoveItem(props) {
    const [inputId, setinputId] = useState();

    function handleIdChange(e) {
        setinputId({inputId: e.target.value});
    }

    return (props.trigger) ? (
        <div className="item-popup">

            <div id={props.id} class="form">

                <div className='item-popup-header'>
                    <p className='item-popup-header-id'>REMOVER PRODUTO</p>

                    <img src={process.env.PUBLIC_URL + '/xmark-solid.svg'} className='item-popup-header-close' onClick={() => props.setTrigger(false)} />
                </div>

                <div class="input-container ic1">
                    <input id={"name-" + props.id} class="input" type="text" placeholder=" " onChange={(e)=> handleIdChange(e)} />
                
                    <label for="name" class="placeholder">ID</label>
                </div>

                <button type="text" class="button" onClick={async ()=>{
                    await axios.delete(`${process.env.REACT_APP_ENDPOINT}/api/v1/product/${parseInt(inputId.inputId)}`)
                    .then((response) => {
                        if(response.status === 200){
                            alert("Produto removido com sucesso!")
                            window.location.reload(false)
                        }else{
                            alert("Ocorreu um erro, tente mais tarde novamente.")
                        }
                        // console.log(response.status)
                    })
                }}>Remover</button>
            </div>
        </div>
    ) : "";
}

export default RemoveItem;