const { DataTypes } = require("sequelize");
const sequelize = require("./index")

const Reports = sequelize.define('Reports', {
  // Model attributes are defined here
  name: {
    type: DataTypes.STRING,
    // allowNull: false
  },
  companyId: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  // Other model options go here
});

module.exports = Reports;