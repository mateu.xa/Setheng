const { DataTypes } = require("sequelize");
const sequelize = require("./index")

const Products = sequelize.define('Products', {
  // Model attributes are defined here
  name: {
    type: DataTypes.STRING,
    // allowNull: false
  },
  description: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  image: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  // Other model options go here
});

module.exports = Products;