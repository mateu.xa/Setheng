const { DataTypes } = require("sequelize");
const sequelize = require("./index")
const crypto = require('crypto')

const Companies = sequelize.define('Companies', {
  // Model attributes are defined here
  companyName: {
    type: DataTypes.STRING,
    // allowNull: false
  },
  email: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  password: {
    type: DataTypes.STRING,
    // set(value) {
    //   // Storing passwords in plaintext in the database is terrible.
    //   // Hashing the value with an appropriate cryptographic hash function is better.
    //   this.setDataValue('password', crypto.createHash(process.env.HASH_KEY).update(value).digest('hex'));
    // }
  },
  cnpj: {
    type: DataTypes.STRING
    // allowNull defaults to true
  }
}, {
  // Other model options go here
});

module.exports = Companies;