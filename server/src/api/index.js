const express = require("express")

const postCompany = require("./company/post")
const getCompany = require("./company/get")
const getAllCompanies = require("./company/getAll")
const deleteCompany = require('./company/delete')
const patchCompany = require('./company/patch')
const loginCompany = require("./company/login")

const postProduct = require('./products/post')
const getProduct = require('./products/get')
const getAllProducts = require('./products/getAll')
const deleteProduct = require('./products/delete')
const patchProduct = require('./products/patch')

const postReport = require('./reports/post')
const getReport = require('./reports/get')
const getAllReports = require('./reports/getAll')
const deleteReport = require('./reports/delete')
const patchReport = require('./reports/patch')

const router = express.Router()

router.use(postCompany)
router.use(getCompany)
router.use(getAllCompanies)
router.use(deleteCompany)
router.use(patchCompany)

router.use(postProduct)
router.use(getProduct)
router.use(getAllProducts)
router.use(deleteProduct)
router.use(patchProduct)

router.use(postReport)
router.use(getReport)
router.use(getAllReports)
router.use(deleteReport)
router.use(patchReport)

module.exports = router