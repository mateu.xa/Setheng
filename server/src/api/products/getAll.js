const express = require("express");
const Products = require("../../models/products");
const { Op } = require('sequelize');

const router = express.Router();

router.get("/products", async (req, res) => {

    const query = String(req.query.query)

    const productId = String(req.query.productId)

    const limit = 
        isNaN(parseInt(req.query.limit))
            ?
                100
            :
                parseInt(req.query.limit)

    const offset = 
        isNaN(parseInt(req.query.cursor)) 
        ? 
            0
        : 
            0 + (parseInt(req.query.cursor) - 1) * limit 

    const where = () => {
        let jsonItems = {}

        if(query !== 'undefined'){
            jsonItems['name'] = {
                [Op.like]: '%' + query + '%'
            }
        }
        if(productId !== 'undefined'){
            jsonItems['companyId'] = productId
        }
        return jsonItems
    }

    const getProducts = await Products.findAll({
        offset: offset,
        limit: limit,
        where: where()
    });

    res.json(getProducts);

});

module.exports = router;