const express = require("express");
const Products = require("../../models/products");

const router = express.Router();

router.patch("/product/:prdId", async (req, res) => {

    const id = req.params.prdId;

    let { name, description } = req.body;

    name = String(name)
    description = String(description)

    const payload = () => {
        let jsonItems = {}

        if(name !== 'undefined'){
            jsonItems['name'] = name
        }
        if(description !== 'undefined'){
            jsonItems['description'] = description
        }

        return jsonItems
    }

    const product = await Products.update(payload(),
        { 
            where: { id } 
        }).catch(
            (err) => {
            res.json({"Error": err}).status(400);
        }
    );

    if(product){
        const patchedProduct = await Products.findOne({ where: { id } }).catch(
            (err) => {
            res.json({"Error": err}).status(400);
            }
        );

        res.json(patchedProduct).status(200)
    }else{
        res.json("Something went wrong").status(404)
    }

});

module.exports = router;