const express = require("express");
const Products = require("../../models/products");
const { multer } = require("../../multer")
const { google } = require("googleapis");
const gal = require('google-auth-library');

const router = express.Router();

const { Readable } = require("stream");

const authenticateGoogle = () => {
  const auth = new gal.GoogleAuth({
    keyFile: './src/service-account-key-file.json',
    scopes: "https://www.googleapis.com/auth/drive",
  });
  return auth;
};

const uploadToGoogleDrive = async (file, fileName, auth) => {
  const fileMetadata = {
    name: fileName,
    parents: ["1-5b3yU-UdbRO-r0CQirv_DC7hGrnIO-f"], // Change it according to your desired parent folder id
  };

  const media = {
    mimeType: file.mimetype,
    body: bufferToStream(file.buffer),
  };

  const driveService = google.drive({ version: "v3", auth });

  const response = await driveService.files.create({
    requestBody: fileMetadata,
    media: media,
    fields: "id",
  });
  return response;
};

const bufferToStream = (buffer) => {
  const stream = new Readable()
  stream.push(buffer)
  stream.push(null)

  return stream
}

router.post("/product", multer.single("file"), async (req, res) => {
  try {
    if (!req.file) {
      res.status(400).send("No file uploaded.");
      return;
    }

    const { name, description } = req.body;

    const image = "image_" + name + "_" + description.substr(0, 2) + 
                    description.substr(description.length -2)

    const alreadyExistsProduct = await Products.findOne({ where: { name } }).catch(
      (err) => {
        res.json({"Error": err}).status(400);
      }
    );

    if (alreadyExistsProduct) {
      return res.status(409).json({ message: "Product with name already exists!" });
    }

    const auth = authenticateGoogle();
    const response = await uploadToGoogleDrive(req.file, image, auth);

    const newProduct = new Products({ name, description, image });

    const savedProduct = await newProduct.save().catch((err) => {
      res.json({"Error": err}).status(400);
      res.status(500).json({ error: "Cannot register product at the moment!" });
    });
  
    if (savedProduct){
  
      const queriedProduct = await Products.findOne({ where: { name } }).catch(
        (err) => {
          res.json({"Error": err}).status(400);
        }
      );
      res.json(queriedProduct);
    }
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;