const express = require("express");
const Products = require("../../models/products");

const router = express.Router();

router.get("/product/:prdId", async (req, res) => {

    const id = req.params.prdId;

    const product = await Products.findOne({ where: { id } }).catch(
        (err) => {
        res.json({"Error": err}).status(400);
        }
    );

    if(product == null){
        res.json("Product not found").status(404)
    }else{
        res.json(product).status(200)
    }
});

module.exports = router;