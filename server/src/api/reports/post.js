const express = require("express");
const Reports = require("../../models/reports");

const router = express.Router();

router.post("/report", async (req, res) => {
  const { name, companyId } = req.body;

  const alreadyExistsReport = await Reports.findOne({ where: { name } }).catch(
    (err) => {
      res.json({"Error": err}).status(400);
    }
  );

  if (alreadyExistsReport) {
    return res.status(409).json({ message: "Report with name already exists!" });
  }

  const newReport = new Reports({ name, companyId });

  const savedReport = await newReport.save().catch((err) => {
    res.json({"Error": err}).status(400);
    res.status(500).json({ error: "Cannot register report at the moment!" });
  });

  if (savedReport){

    const queriedReport = await Reports.findOne({ where: { name } }).catch(
      (err) => {
        res.json({"Error": err}).status(400);
      }
    );

    res.json(queriedReport);
  }

});

module.exports = router;