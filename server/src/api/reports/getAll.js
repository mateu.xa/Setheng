const express = require("express");
const Reports = require("../../models/reports");
const { Op } = require('sequelize');

const router = express.Router();

router.get("/reports", async (req, res) => {

    const query = String(req.query.query)

    const companyId = String(req.query.companyId)

    const limit = 
        isNaN(parseInt(req.query.limit))
            ?
                100
            :
                parseInt(req.query.limit)

    const offset = 
        isNaN(parseInt(req.query.cursor)) 
        ? 
            0
        : 
            0 + (parseInt(req.query.cursor) - 1) * limit 


    const where = () => {
        let jsonItems = {}

        if(query !== 'undefined'){
            jsonItems['name'] = {
                [Op.like]: '%' + query + '%'
            }
        }
        if(companyId !== 'undefined'){
            jsonItems['companyId'] = companyId
        }
        return jsonItems
    }

    const getReports = await Reports.findAll({
            offset: offset,
            limit: limit,
            where: where()
        });

    res.json(getReports);

});

module.exports = router;