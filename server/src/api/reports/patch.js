const express = require("express");
const Reports = require("../../models/reports");

const router = express.Router();

router.patch("/report/:repId", async (req, res) => {

    const id = req.params.repId;

    let { name, companyId } = req.body;

    name = String(name)
    companyId = String(companyId)

    const payload = () => {
        let jsonItems = {}

        if(name !== 'undefined'){
            jsonItems['name'] = name
        }
        if(companyId !== 'undefined'){
            jsonItems['companyId'] = companyId
        }

        return jsonItems
    }

    const report = await Reports.update(payload(),
        { 
            where: { id } 
        }).catch(
            (err) => {
            res.json({"Error": err}).status(400);
        }
    );

    if(report){
        const patchedReport = await Reports.findOne({ where: { id } }).catch(
            (err) => {
            res.json({"Error": err}).status(400);
            }
        );

        res.json(patchedReport).status(200)
    }else{
        res.json("Something went wrong").status(404)
    }

});

module.exports = router;