const express = require("express");
const Products = require("../../models/products");

const router = express.Router();

router.delete("/product/:prdId", async (req, res) => {

    const id = req.params.prdId;

    const product = await Products.destroy({ where: { id } }).catch(
        (err) => {
            res.json({"Error": err}).status(400);
        }
    );
    res.json("Product successfully deleted!").status(200);

});

module.exports = router;