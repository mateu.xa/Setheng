const express = require("express");
const Companies = require("../../models/companies");

const router = express.Router();

router.get("/company/:cmpId", async (req, res) => {

    const id = req.params.cmpId;

    const product = await Companies.findOne({ where: { id } }).catch(
        (err) => {
            res.json({"Error": err}).status(400);
        }
    );

    if(product == null){
        res.json("Company not found").status(404)
    }else{
        res.json(product).status(200)
    }
});

module.exports = router;