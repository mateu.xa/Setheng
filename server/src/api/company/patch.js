const express = require("express");
const Companies = require("../../models/companies");

const router = express.Router();

router.patch("/company/:cmpId", async (req, res) => {

    const id = req.params.cmpId;

    let { companyName, email, password, cnpj } = req.body;

    companyName = String(companyName)
    email = String(email)
    password = String(password)
    cnpj = String(cnpj)

    const payload = () => {
        let jsonItems = {}

        if(companyName !== 'undefined'){
            jsonItems['companyName'] = companyName
        }
        if(email !== 'undefined'){
            jsonItems['email'] = email
        }
        if(password !== 'undefined'){
            jsonItems['password'] = password
        }
        if(cnpj !== 'undefined'){
            jsonItems['cnpj'] = cnpj
        }

        return jsonItems
    }

    const product = await Companies.update(payload(),
        { 
            where: { id } 
        }).catch(
            (err) => {
            res.json({"Error": err}).status(400);
        }
    );

    if(product){
        const patchedProduct = await Companies.findOne({ where: { id } }).catch(
            (err) => {
            res.json({"Error": err}).status(400);
            }
        );

        res.json(patchedProduct).status(200)
    }else{
        res.json("Something went wrong").status(404)
    }
});

module.exports = router;