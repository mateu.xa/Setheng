const express = require("express");
const Companies = require("../../models/companies");

const router = express.Router();

router.post("/company", async (req, res) => {
  const { companyName, email, password, cnpj } = req.body;

  const alreadyExistsCompany = await Companies.findOne({ where: { email } }).catch(
    (err) => {
      console.log("Error: ", err);
    }
  );

  if (alreadyExistsCompany) {
    return res.status(409).json({ message: "Company with email already exists!" });
  }

  const newCompany = new Companies({ companyName, email, password, cnpj });

  const savedCompany = await newCompany.save().catch((err) => {
    console.log("Error: ", err);
    res.status(500).json({ error: "Cannot register company at the moment!" });
  });

  if (savedCompany) res.json({ message: "Thanks for registering" });
});

module.exports = router;