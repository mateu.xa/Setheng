const express = require("express");
const Company = require("../../models/companies.js");
const jwt = require("jsonwebtoken");

const router = express.Router();

router.get("/company", async (req, res) => {
  const { companyName, email, password, cnpj } = req.body;

  const companyWithEmail = await Company.findOne({ where: { email } }).catch(
    (err) => {
      console.log("Error: ", err);
    }
  );

  if (!companyWithEmail)
    return res
      .status(400)
      .json({ message: "Email or password does not match!" });

  if (companyWithEmail.password !== password)
    return res
      .status(400)
      .json({ message: "Email or password does not match!" });

  const jwtToken = jwt.sign(
    { id: companyWithEmail.id, email: companyWithEmail.email },
    process.env.JWT_SECRET
  );

  res.json({ message: "Welcome Back!", token: jwtToken });
});

module.exports = router;