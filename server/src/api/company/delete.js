const express = require("express");
const Companies = require("../../models/companies");

const router = express.Router();

router.delete("/company/:cmpId", async (req, res) => {

    const id = req.params.cmpId;

    const company = await Companies.destroy({ where: { id } }).catch(
        (err) => {
            res.json({"Error": err}).status(400);
        }
    );
    res.json("Company successfully deleted!").status(200);

});

module.exports = router;