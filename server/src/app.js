const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// require("./auth/passport");
require("./models/companies")
require("./models/products")
require("./models/reports")

const middlewares = require("./middleware");
const api = require("./api");

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());
app.use(express.json());

app.post("/", async (req, res) => {
});

app.get("/", async(req, res) => {

  bucketName = 'technical-report',
  fileName = 'testandoID.txt',
  destFileName = './downloaded.txt'

  const options = {
    destination: destFileName,
  };

  // Downloads the file
  await storage.bucket(bucketName).file(fileName).download(options);

  console.log(
    `gs://${bucketName}/${fileName} downloaded to ${destFileName}.`
  );
})

app.use("/api/v1", api);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;